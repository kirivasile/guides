### Инструкция по сборке примеров для Windows ###
* Для начала выполните все пункты, приведённые тут https://alexander-bobkov.ru/opengl/samples
* Создайте в папке *D:\Graphics* следующие папки:
	- *tasks* - здесь будет выкачан репозиторий со сданными заданиями
	- *tasks-build* - здесь будет находится solution для ваших проектов
	- *tasks-install* - здесь будут храниться исполняемые файлы ваших проектов
* Выкачайте [репозиторий](https://bitbucket.org/AlexBobkov/studenttasks2017) в папку *tasks*
* Перейдите в папку *task1* и создайте там подпапку `<номер группы><фамилия на латинице>` (например, *123Ivanov*) и скопируйте в неё файлы с исходным кодом *.h* и *.cpp* 
* В папке `<номер группы><фамилия на латинице>` создайте файле CMakeLists.txt следующего содержимого
```
set(SRC_FILES
    Main.h
    Main.cpp
)

MAKE_TASK(123Ivanov 1 "${SRC_FILES}")
```
* Откройте cmake-gui. Если в нём уже имеется какой-то вывод, то нужно удалить кеш: *File -> Delete Cache*. В поле source code укажите *D:/Graphics/tasks*, в следующем поле *D:/Graphics/tasks-build*
* Нажмите **Configure**. Выйдет ошибка. В полученном выводе установите следующие переменные:
	* DEPENDENCIES_ROOT - *D:/Graphics/dependencies-install*
	* CMAKE_INSTALL_PREFIX - *D:/Graphics/tasks-install*
	* SELECTED_STUDENT - `<номер группы><фамилия на латинице>` (например, *123Ivanov*)
* Нажимаете **Configure**, а затем **Generate** и **Open Project**.
* Чтобы запустить проект выберите свой проект в Solution Explorer(Обозреватель решений), нажмить Build(Собрать). Затем аналогично с проектом INSTALL. Исполняемый файл появится в папке *D:/Graphics/tasks-install*
* Чтобы запускать и дебажить проекты в Visual Studio. В Solution Explorer нажмите правой кнопкой на свой проект, и выберите Set as StartUpProject(Сделать проект запускаемым). В папку *D:/Graphics/tasks-build/task1/123Ivanov/Debug* поместите файлы *glfw3.dll, glfw3d.dll*, которые можно найти в *D:/Graphics/dependencies-install/lib*. Теперь можете запускать и дебажить свои проекты в Visual Studio.
* Для сдачи задания следуйте этому [туториалу](https://bitbucket.org/AlexBobkov/studenttasks2017).